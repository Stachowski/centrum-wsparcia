let baseUrl;


export async function $fetch(url, options) {
    const finalOptions = Object.assign({}, {
        headers:{
            'Content-Type': 'application/json',
        },
        //przesyłane dane służące do logowania
        credentials: 'include',
    }, options)

    const response = await fetch(`${baseUrl}${url}`, finalOptions)
    if (response.ok) {
        const data = await response.json()
        return data
    } else {

        const message = await response.text()
        const error = new Error(message);
        error.response = response
        throw error
    }
}

//export domyślny obiektu z metoda install
export default {
    //przekazujemy parametr Vue oraz parametr options
    install(Vue, options){
        console.log('Plugin zainstalowany', options);
        //
        baseUrl = options.baseUrl;

        Vue.prototype.$fetch = $fetch
    }
}
