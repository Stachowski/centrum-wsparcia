export default {
    install(Vue, state){
        Object.defineProperty(Vue.prototype, '$state', {
            //get zwraca stan
            get: () => state,
        })
    }
}