import Vue from 'vue'
import Loading from './components/Loading.vue'
import SmartForm from './components/SmartForm.vue'
import FormInput from './components/FormInput.vue'

//rejestrujemy globalnie  komponent(nazwa komponentu i komponent)
Vue.component('Loading', Loading);
Vue.component('SmartForm', SmartForm);
Vue.component('FormInput', FormInput);