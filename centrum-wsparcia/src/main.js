import 'babel-polyfill'
import Vue from 'vue'

//import VueFetch from './plugins/fetch.js'

import VueState from './plugins/state.js'

import AppLayout from './components/AppLayout.vue'

import router from './router.js'
import './global-components.js'

import VueFetch, { $fetch } from './plugins/fetch'
import state from './state.js'


//ponagla do korzystania z naszego pluginu
Vue.use(VueFetch,{
    //obiekt, ktory będziemy przekazywać
    //w tym wypadku jest to adres serwera
    baseUrl: 'http://localhost:3000/',
});

Vue.use(VueState, state)


async function main() {
    try{
        //pobiera info o tym czy user jest zalogowany
        state.user = await $fetch('user')
    } catch (e) {
        console.warn(e)
    }
}



new Vue({
    el: '#app',
    //przypisujemy state do data tak aby był on reaktywny
    data: state,
    render: h => h(AppLayout),
    router
});

main()