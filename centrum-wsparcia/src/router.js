import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from './components/Home.vue'
import FAQ from './components/FAQ.vue'
import Login from './components/Login.vue'
import TicketsLayout from './components/TicketsLayout.vue'

import state from './plugins/state.js'

Vue.use(VueRouter);




const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/faq',
        name: 'faq',
        component: FAQ
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            quest: true
        }
    },
    //ścieżka dostępna tylko dla zalogowanych użytkowników
    {
        path: '/tickets',
        name: 'tickets',
        component: TicketsLayout,
        //tak ją prywatyzujemy
        meta:{
            private: true
        }
    }
];

//toworzymy stałą router która tworzy nową instację obiektu vuerouter i do niej przekazujemy tablice ze ścieżkami
const router = new VueRouter({
    routes,
    mode: 'history'
})

//ochronne mechanizmy
//przed każdym przejściem
router.beforeEach((to, from, next) => {
    //sprawdzamy czy ścieżka jest prywatna oraz czy użytkownik nie jest zalogowany
    if(to.meta.private && !state.user){
        //przekierowuje do zalogowania
        next({
            name: 'login',
            params: {
                //to po to aby po zalogoowaniu przekierować nas tam gdzie nie mieliśmy dostępu a chcieliśmy wbić
                wantedRoute: to.fullPath
            }
        })
        return
    }
    //czy user jest zalogowany i czy ścieżka nie pozwala na uruchamianie kiedy jest użytkownik zalogowany
    if(to.meta.quest && state.user){
        next({
            name: 'home'

        })
        return
    }
    next()
})

//export domyślny
export default router