export default function(resources){
    return{

        computed:{
          remoteDataBusy(){
              return this.$data.remoteDataLoading
          },

            hasRemoteErrors(){
              //some to metoda
              return Object.keys(this.$data.remoteErrors).some(
                  key => this.$data.remoteErrors[key]
              )
            }
        },


        data(){
            let initData = {
                //do liczenia ilości zapytań
                remoteDataLoading: 0,
            };


            initData.remoteErrors = {}


            //zmienna będzie przeszukiwać w zasobach
            for(const key in resources){
                initData[key] = null

                initData.remoteErrors[key] = null
            }

            return initData
        },
        methods:{
            async fetchResource(key, url){

                this.$data.remoteDataLoading++;
                //resetuje błędy
                this.$data.remoteErrors[key] = null;

                try{
                    this.$data[key] = await this.$fetch(url)
                } catch(e){
                    console.error(e)
                    //w przypadku wystąpienia blędy dodaje go
                    this.$data.remoteErrors[key] = e
                }

                this.$data.remoteDataLoading--;

            }
        },
        created(){

            for(const key in resources){
                let url = resources[key];
                //wywołanie metody
                this.fetchResource(key, url)
            }
        }
    }
}